# -*- coding: utf-8 -*-
# StreamOnDemand Community Edition - Kodi Addon
# ------------------------------------------------------------
# streamondemand.- XBMC Plugin
# Canale guardogratis
# http://www.mimediacenter.info/foro/viewforum.php?f=36
# Version: 201802240900
# ------------------------------------------------------------
import re  
import urlparse

from core import config, httptools
from platformcode import logger
from core import scrapertools
from core import servertools
from core.item import Item
from core.tmdb import infoSod

__channel__ = "guardogratis"

host = "https://guardogratis.com/"

headers = [['Referer', host]]

def mainlist(item):
    logger.info("[guardogratis.py] mainlist")

    # Main options
    itemlist = [Item(channel=__channel__,
                     action="list_titles",
                     title="[COLOR azure]Film[/COLOR]",
                     url="%s/movies/" % host,
                     extra="movie",
                     thumbnail="http://orig03.deviantart.net/6889/f/2014/079/7/b/movies_and_popcorn_folder_icon_by_matheusgrilo-d7ay4tw.png"),
                Item(channel=__channel__,
                     action="list_titles",
                     title="[COLOR azure]Top Film[/COLOR]",
                     url="%s/top-imdb/" % host,
                     extra="movie",
                     thumbnail="http://orig03.deviantart.net/6889/f/2014/079/7/b/movies_and_popcorn_folder_icon_by_matheusgrilo-d7ay4tw.png"),
                Item(channel=__channel__,
                     action="categorie",
                     title="[COLOR azure]Categorie[/COLOR]",
                     url="%s" % host,
                     extra="categorie",
                     thumbnail="http://orig03.deviantart.net/6889/f/2014/079/7/b/movies_and_popcorn_folder_icon_by_matheusgrilo-d7ay4tw.png"),
                Item(channel=__channel__,
                     action="list_titles",
                     title="[COLOR azure]Serie Tv[/COLOR]",
                     url="%s/series/" % host,
                     extra="serie",
                     thumbnail="http://orig03.deviantart.net/6889/f/2014/079/7/b/movies_and_popcorn_folder_icon_by_matheusgrilo-d7ay4tw.png"),
                Item(channel=__channel__,
                     action="search",
                     title="[COLOR yellow]Cerca Film[/COLOR]",
                     extra="movie",
                     thumbnail="http://dc467.4shared.com/img/fEbJqOum/s7/13feaf0c8c0/Search"),
                Item(channel=__channel__,
                     action="search",
                     title="[COLOR yellow]Cerca SerieTV[/COLOR]",
                     extra="serie",
                     thumbnail="http://dc467.4shared.com/img/fEbJqOum/s7/13feaf0c8c0/Search")]

    return itemlist

def list_titles(item):
    logger.info("[guardogratis.py] list_titles")
    itemlist = []
    
    tipo='movie'
    if 'serie' in item.extra: tipo='tv'

    if item.url == "":
        item.url = host
    # Carica la pagina 
    data = httptools.downloadpage(item.url, headers=headers).data

    patronvideos = '<div data-movie-id=.*?href="([^"]+)".*?data-original="([^"]+)".*?<h2>([^<]+)<\/h2>.*?[I,T]MDb:\s*([^<]+)<\/div>'

    matches = re.compile(patronvideos, re.DOTALL).finditer(data)
    
    for match in matches:
        scrapedurl = urlparse.urljoin(item.url, match.group(1))
        scrapedthumbnail = urlparse.urljoin(item.url, match.group(2))
        scrapedthumbnail = scrapedthumbnail.replace(" ", "%20")
        rate='  IMDb: [[COLOR orange]%s[/COLOR]]' % match.group(4) if match.group(4)!='N/A'else ''
        scrapedtitle = scrapertools.unescape(match.group(3))
        #scrapedtitle = scrapertools.unescape(match.group(3))+rate
        itemlist.append(infoSod(
            Item(channel=__channel__,
                 action="findvideos" if not 'serie' in item.extra else 'serietv',
                 contentType="movie" if not 'serie' in item.extra else 'serie',
                 fulltitle=scrapedtitle,
                 show=scrapedtitle,
                 title=scrapedtitle,
                 url=scrapedurl,
                 thumbnail=scrapedthumbnail,
                 extra=item.extra,
                 viewmode="movie_with_plot"), tipo=tipo))

    nextpage_regex=''
    if item.extra in "movies,serietv":
        nextpage_regex='<div id="pagination" style="margin: 0;">.*?active.*?href=\'([^\']+)\'.*?</div>'
    elif item.extra=="categorie":
        nextpage_regex='<li class=\'active\'>.*?href=\'([^\']+)\'.*?</a></li>'

    if nextpage_regex:
        next_page = scrapertools.find_single_match(data, nextpage_regex)
        if next_page != "":
            itemlist.append(
                Item(channel=__channel__,
                     action="list_titles",
                     title="[COLOR orange]Successivo >>[/COLOR]",
                     url="%s" % next_page,
                     extra=item.extra,
                     thumbnail="http://2.bp.blogspot.com/-fE9tzwmjaeQ/UcM2apxDtjI/AAAAAAAAeeg/WKSGM2TADLM/s1600/pager+old.png"))

    return itemlist

def search(item, texto):
    logger.info("[guardogratis.py] search")
    item.url = host + "/?s=" + texto
    try:
        if item.extra == "movie":
            return list_titles(item)
        if item.extra == "serie":
            return list_titles(item)
    # Continua la ricerca in caso di errore 
    except:
        import sys
        for line in sys.exc_info():
            logger.error("%s" % line)
        return []

def categorie(item):
    logger.info("[guardogratis.py] categorie")
    itemlist = []

    if item.url == "":
        item.url = host

    # Carica la pagina 
    data = httptools.downloadpage(item.url, headers=headers).data
    patronvideos = '<li id="menu-item-.*?category.*?href="([^"]+)">([^"]+)</a>'

    matches = re.compile(patronvideos, re.DOTALL).finditer(data)
    
    for match in matches:
        scrapedurl = urlparse.urljoin(item.url, match.group(1))
        scrapedtitle = match.group(2)
        itemlist.append(
            Item(channel=__channel__,
                 action="list_titles",
                 title=scrapedtitle,
                 url=scrapedurl,
                 extra=item.extra,
                 folder=True))

    return itemlist


def serietv(item):
    logger.info("[guardogratis.py] serietv")

    itemlist = []

    data = httptools.downloadpage(item.url, headers=headers).data

    patron = '<a href="([^"]+)">Episode[^<]+</a>'
    matches = re.compile(patron, re.DOTALL).findall(data)

    for scrapedurl in matches:
        scrapedplot = ""
        scrapedthumbnail = ""
        scrapedtitle = scrapedurl
        scrapedtitle = scrapedtitle.replace(host, "")
        scrapedtitle = scrapedtitle.replace("episode/", "")
        scrapedtitle = scrapedtitle.replace("/", "")
        scrapedtitle = scrapedtitle.replace("-", " ")
        scrapedtitle = scrapedtitle.title()
        itemlist.append(infoSod(
            Item(channel=__channel__,
                 action="findvideos",
                 fulltitle=scrapedtitle,
                 show=scrapedtitle,
                 title="[COLOR azure]" + scrapedtitle + "[/COLOR]",
                 url=scrapedurl,
                 thumbnail=scrapedthumbnail,
                 plot=scrapedplot,
                 folder=True), tipo='tv'))

    if config.get_library_support() and len(itemlist) != 0:
        itemlist.append(
            Item(channel=__channel__,
                title="Aggiungi alla libreria",
                url=item.url,
                action="add_serie_to_library",
                extra="serietv",
                show=item.show))

    return itemlist

def findvideos(item):
    logger.info("[guardogratis.py] findvideos")

    # Carica la pagina 
    data = httptools.downloadpage(item.url, headers=headers).data

    itemlist = servertools.find_video_items(data=data)
    for i in itemlist:
        tab=re.compile('<div\s*id="(tab[^"]+)"[^>]+>[^>]+>[^>]+src="http[s]*:%s[^"]+"'%i.url.replace('http:','').replace('https:',''), re.DOTALL).findall(data)
        qual=''
        if tab:
            qual=re.compile('<a\s*href="#%s">([^<]+)<'%tab[0], re.DOTALL).findall(data)[0].replace("'","")
            qual="[COLOR orange]%s[/COLOR] - "%qual
        i.title='%s[COLOR green][B]%s[/B][/COLOR] - %s'%(qual,i.title[2:],item.title)
        i.channel=__channel__
        i.fulltitle=item.title

    return itemlist

