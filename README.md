# Fork of the [Stream On Demand](https://github.com/streamondemand/plugin.video.streamondemand) plugin for Kodi

Considering that the creators decided to stop the development of streamondemand I decided to fork the project here so that, even though I don't have time to continue it, maybe someone else can do it!

**To contribute**: please write to me (or simply open an issue) and I will add you to contributors!

(Is there any other way to let anyone collaborate?)

Fork of the italian STREAM ON DEMAND plugin for Kodi



